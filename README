This tool aims to centralize the reweights done in the Ztag analysis. 
The tool to obtain the weight for each event as well as the files are included
in the respository and any update will be propagated to the group in case any
change is done. 
Instructions to used the tool follow:

Compilation:

- Compile the code with the Makefile included just doing make.
- Once compiled link it to your code as usual.


Use:

- Declare a RewTool object:

RewTool *tool = new RewTool(mode,sample,channel,syst)

The mode variable is a string used to define de reweight we want to do. There are three modes:
    * zpt: The first zpt reweight which is applied only to Alpgen.
    * nbjets: The scaling for b-jet multiplicity.
    * 1tagzpt: The last reweight in the Pt(Z) done in the one tag bin.

The sample variable is used to define the sample to which the rweight will be applied: Sherpa or Alpgen.

The channel variable will be ee or mm.

The syst variable is used when the reweight will be applied running over some systematic. This variable
should take the same name of the systematic which is written in the ntuple name, the ones which are defined
by TRC. There are two special cases:
    * syst=nbjetsRew => This is used when you want to evaluate the systematic associated with the effect of
    deriving the nbjets scaling in a control regio with pt(Z) < 50 GeV.
    * syst = 1tagRew => This is the systematic asociated to the control region of the pt(Z) reweight. In this case
    the reweight derived in the 0 tag region will be applied.

This tool will load everything needed to get the reweight for each event. 

To get the weight of each event just do:

double weight = tool->getRew(par);

where par will be the parameter used for the reweight (Pt(Z) in GeV or b jet multiplicity)

You can also get the error of the reweight with the function getRewError(par). It is used
in the same way as before and return the error of the reweight for a given parameter.
