#include "RewTool.h"
#include <sstream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TStyle.h"
#include "TF1.h"
#include "TMatrixDSym.h"
#include "TVirtualFitter.h"

// Define used only for development to obtain the plots for the reweights
// If you want to get the plots:
//  - Uncomment the define line
//  - Add a call to the static functino RewTool::setStyle() 
//  once in the beginning of the code where you use RewTool.
//#define DOPLOT

//Include files for plots
#ifdef DOPLOT
#include "plotFiles/AtlasUtils.cxx"
#include "plotFiles/AtlasLabels.cxx"
#include "plotFiles/AtlasStyle.cxx"
#endif
using namespace std;

int RewTool::atlasStyleDone = false;

/*
 * Constructor (define file and histogram you want to use)
 */
RewTool::RewTool(string file, string histoname):m_fitDone(false){
    init(file,histoname);
}

/*
 * Constructor (define the reweight mode you want to load. It is not dynamic but easier if you don't have to derive and work with histograms)
 */
RewTool::RewTool(string mode,string sample,string ch,string syst):m_fitDone(false){
    //  Define differet histograms and files for different modes
    if (mode != "zpt" && mode != "nbjets" && mode != "1tagzpt"){
        cerr<<"ERROR: The mode supplied "<<mode<<" for RewTool is not supported. Use: zpt, nbjets or 1tagzpt."<<endl;
        exit(1);
    }
    if (sample != "Sherpa" && sample != "Alpgen"){
        cerr<<"ERROR: The sample supplied "<<sample<<" for RewTool is not supported. Only Sherpa and Alpgen are superted"<<endl; 
        exit(1);
    }
    if (ch != "ee" && ch != "mm"){
        cerr<<"ERROR: The channel supplied "<<sample<<" for RewTool is not supported. Use ee and mm"<<endl; 
        exit(1);
    }
    string file = __ABSFILE__;
    if (syst == "nbjetsRew"){
        mode = "nbjets50";
        syst="";
    }
    if (syst == "1tagRew"){
        mode = "0tagzpt";
        syst="";
    }
    file += "/files/RewHistos_"+mode+".root";
    string histo = sample+"_"+ch;
    if (syst != "" && syst != "nominal") histo+="_"+syst;
    init(file,histo);
}

/*
 * Desctructor
 */
RewTool::~RewTool(){
}

void RewTool::init(string file, string histoname){
    // Read the file and load the histogram
    m_file = file;
    //m_useFit = (m_file.find("nbjets") != string::npos) ? false : true;
    m_useFit = (m_file.find("nbjets") != string::npos || m_file.find("deltarzj") != string::npos) ? false : true;
    m_histoName = histoname;

    TFile *f = TFile::Open(file.c_str());
    if (f->IsZombie()){
        cerr<<"File "<<file<<" is marked as zombie"<<endl;
        exit(1);
    }
    if (f->FindKey(histoname.c_str()) == 0){
        cerr<<"ERROR: Histogram for reweight "<<histoname<<" does not exist"<<endl;
        exit(1);
    }
    m_histo = (TH1D*)f->Get(histoname.c_str());
    m_nbins = m_histo->GetNbinsX();
    m_xmin = m_histo->GetBinLowEdge(1);
    m_xmax = m_histo->GetBinLowEdge(m_nbins+1);
    m_errorHisto = NULL;
    cout<<"Info in <RewTool::init>: Opening the root file "<<f->GetName()<<" and the histogram "<<m_histo->GetName()<<" to derive the reweight."<<endl;

}

/*
 * Get the weight to be applied
 */
double RewTool::getRew(double value,string opt,const double cl){
    if(value > 520) value = 520;
    int bin = m_histo->FindBin(value);
    if (bin > m_histo->GetNbinsX()) bin = m_histo->GetNbinsX();
    if(m_useFit){//The 520 if only have effect on ptz
        if (!m_fitDone) fit(opt,cl);
        //return m_fitResult->Parameter(0)+value*m_fitResult->Parameter(1);
        if (m_histo->GetBinContent(bin) < 0) return 0;
        return m_function->Eval(value);
    } else {
        //int bin = (int b = m_histo->FindBin(value) > m_histo->GetNbinsX()) ? b-1 : b;
        /*return m_histo->GetBinContent((bin > m_histo->GetNbinsX()) ? bin-1 : bin);*/
        return (m_histo->GetBinContent(bin) < 0) ? 0 : m_histo->GetBinContent(bin);
    }
}

/*
 * Get the error of the weight. It is used for systematics
 */
double RewTool::getRewError(double value,string opt,const double cl){
    if(value > 520) value = 520;
    if(m_useFit){//The 520 if only have effect on ptz
        if (!m_fitDone) fit(opt,cl);
        int bin = m_errorHisto->FindBin(value);
        if (bin > m_errorHisto->GetNbinsX()) bin = m_errorHisto->GetNbinsX();
        return m_errorHisto->GetBinError(bin);
    } else {
        int bin = m_histo->FindBin(value);
        if (bin > m_histo->GetNbinsX()) bin = m_histo->GetNbinsX();
        return m_histo->GetBinError(bin);
    }
}

/*
 * Plot the histogram of the reweight with the linear fit
 */
void RewTool::fit(string mode, const double cl){
    m_function = new TF1("fit",mode.c_str(),0,800);
    m_fitResult = m_histo->Fit("fit","0SQ");
    int nbins = int(m_xmax - m_xmin);
    m_errorHisto = new TH1D("CLHistogram","",nbins,m_xmin,m_xmax);
    cout<<"Confident level used for the fit: "<<cl<<endl;
    (TVirtualFitter::GetFitter())->GetConfidenceIntervals(m_errorHisto, cl);
    m_fitDone = true;



    #ifdef DOPLOT
    TCanvas *c1 = new TCanvas(m_file.c_str(),m_histoName.c_str(),800,600);
    // Xaxis names
    string xName,sample, channel;
    string file = "RewPlot_";
    string tmp = m_file.substr(m_file.find("_")+1,m_file.find(".root")-m_file.find("_")-1);
    file += tmp + "_";
    double max, min;
    if (m_file.find("zpt") != string::npos){
        xName = "p_{T}(Z) [GeV]";
        max = 2;
        min = 0.3;
    }
    if (m_file.find("deltaphi") != string::npos) xName = "#Delta#Phi (j_1j_2)";
    if (m_file.find("nbjets") != string::npos){
        xName = "N b-jets (p_{T}(Z) < 100 GeV)";
        max = 3;
        min = 0;
    }
    // Samples
    if (m_histoName.find("Alpgen") != string::npos) sample = "Alpgen";
    else sample = "Sherpa";
    // Channel
    if (m_histoName.find("ee") != string::npos){
        channel = "ee channel";
        file += sample+"_ee.pdf";
    }
    else if (m_histoName.find("mm") != string::npos){
        channel = "#mu#mu channel";
        file += sample+"_mm.pdf";
    }
    else {
        channel = "ee+#mu#mu channel";
        file += sample+"_elemu.pdf";
    }
    TMatrixDSym cov = m_fitResult->GetCovarianceMatrix();
    cout<<"Covariance matrix for "<<file<<endl;
    cout<<cov(1,1)<<endl;
    cov.Print();
    // Define histogram properties
    m_histo->SetMaximum(max);
    m_histo->SetMinimum(min);
    m_histo->SetMarkerColor(kBlack);
    m_histo->SetMarkerStyle(kFullCircle);
    m_histo->SetXTitle(xName.c_str());
    m_histo->SetYTitle("Reweight value");
    m_histo->Draw("p");
    m_errorHisto->SetFillStyle(3013);
    m_errorHisto->SetFillColor(kGreen+3);
    m_errorHisto->SetMarkerSize(0);
    m_errorHisto->Draw("e5 same");
    m_function->SetLineColor(kBlue);
    m_function->SetLineStyle(2);
    m_function->Draw("same");

    ATLAS_LABEL(0.24,0.85);
    myText(0.36,0.85,1,"Internal",0.050);
    myText(0.24,0.8,1,(sample+" correction").c_str(),0.04);
    myText(0.24,0.75,1,channel.c_str(),0.04);
    // set legend sizes
    float leg_height = 0.2;
    float leg_width = 0.5;
    float leg_xoffset = 0.65;
    float leg_yoffset = 0.8;
    TLegend leg(leg_xoffset,leg_yoffset-leg_height/2,leg_xoffset+leg_width,leg_yoffset+leg_height/2);
    leg.SetFillColor(0);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.033);

    leg.AddEntry(m_histo,"Reweight","pe");
    leg.AddEntry(m_function,"Fit","l");
    leg.AddEntry(m_errorHisto,"Uncertainty","f");

    leg.Draw("same");

    stringstream xi;
    xi<<"#chi ^{2} = "<<m_fitResult->Chi2();
    stringstream ndf;
    ndf<<"Ndf = "<<m_fitResult->Ndf();
    myText(0.24,0.7,1,xi.str().c_str(),0.04);
    myText(0.24,0.65,1,ndf.str().c_str(),0.04);

    c1->Print(file.c_str());
    delete c1;
    #endif
}

void RewTool::setStyle(){
#ifdef DOPLOT
    if (!atlasStyleDone) SetAtlasStyle();
    atlasStyleDone = true;
#endif
}
