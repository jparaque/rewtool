#ifndef REW_TOOL_H
#define REW_TOOL_H
#include "TH1D.h"
#include "TFile.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TF1.h"
#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>


class RewTool {
public:
    RewTool (std::string tfile, std::string histoName);
    RewTool (std::string mode, std::string sample, std::string channel,std::string syst = "");
    ~RewTool ();

    double getRew(double value,std::string opt="pol3", const double = 0.95);
    double getRewError(double value,std::string opt="pol3",const double = 0.95);
    void init(std::string file, std::string histoname);

    static void setStyle();

private:
    void fit(std::string mode, const double cl);
    std::vector<double> m_central;//!< Central value. Applied to reweight
    std::vector<double> m_error;//!< Erro in the weight
    std::string m_file;//!< Name of the file to open
    std::string m_histoName;//!< Name of the histogram to use
    TH1D *m_histo;//!< Histogram used to reweight.
    TH1D *m_errorHisto;//!< Histogram holding the c.l. for the fit in the errors.
    TFitResultPtr m_fitResult;//!< Result of the linear fit
    TF1 *m_function;//!< Function used for the fit
    bool m_fitDone;//!< Flag set true if the fit has already been done
    bool m_useFit;//!< Flag set to true if the fit needs to be done. This is decided based on the reweight mode.
    static int atlasStyleDone;
    double m_xmax, m_xmin;
    int m_nbins;

};
#endif
