SHELL = /bin/bash

CXX        = g++

ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS      = $(shell $(ROOTSYS)/bin/root-config --libs)

CXXFLAGS   = -O2 -Wall $(ROOTCFLAGS)

## Dyn flags ##
UNAME = $(shell uname)

ifeq ($(UNAME), Linux)
	DYNFLAGS = -shared -Wl,-soname=$(PWD)/$@
endif

ifeq ($(UNAME), Darwin)
	DYNFLAGS = -dynamiclib -install_name $(PWD)/$@
endif

LIBS	   = $(ROOTLIBS) 
INCLUDES = -I$(ROOTSYS)/include


################################################################################
# Rules
################################################################################

all: libRewTool.so

RewTool.o: RewTool.cxx RewTool.h
	@echo Compiling...
	$(CXX) -fPIC $(CXXFLAGS) $(INCLUDES) -D__ABSFILE__='"$(shell pwd)"' -c $< -o $@

libRewTool.so: RewTool.o
	$(CXX) $(CXXFLGAS) $(DYNFLAGS) $(LIBS) -o $@ RewTool.o


clean:
	rm -rf *.o *.so
